package controllers;

import play.mvc.Controller;
import play.mvc.Result;

import views.html.cancion.*;

public class CancionController extends Controller{

	public Result agregar(){
		return ok(agregar.render("Agregar Canción"));
	}
	
	public Result eliminar(){
		return ok(eliminar.render("Eliminar Canción"));
	}
	
}
