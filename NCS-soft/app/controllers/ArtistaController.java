package controllers;

import play.mvc.Controller;
import play.mvc.Result;

import views.html.artista.*;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import models.*;

public class ArtistaController extends Controller {

	Artista artist = new Artista();
	
	public Result crear(){		
		artist.generarPrueba();
		return ok(crear.render("Crear Artista"));
	}
	
	public Result modificar(){
		return ok(modificar.render("Modificar Artista"));
	}
	
	public Result eliminar(){
		return ok(eliminar.render("Eliminar Artista"));
	}
	
	public Result listarCanciones(){
		String cadena = artist.getNombre();
		ArrayList<Cancion> canciones = artist.getCanciones();
		
		for(int i=0; i<canciones.size(); i++){
			Cancion can = canciones.get(i);
			cadena = cadena.concat("Nombre: "+can.getNombre()+" URL: "+can.getUrl());
		}
		
		return ok(listarCanciones.render(cadena));
	}
	
	
}
