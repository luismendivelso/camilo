package models;

import java.util.ArrayList;

public class Artista {

	private String nombre;
	private String nacionalidad;
	private String paginaWeb;
	private ArrayList<Cancion> canciones;
	
	
	public Artista(){
		canciones = new ArrayList<Cancion>();
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNacionalidad() {
		return nacionalidad;
	}
	
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	public String getPaginaWeb() {
		return paginaWeb;
	}
	
	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}
	
	public void agregarCanción(Cancion cancion){
		canciones.add(cancion);
	}
	
	public ArrayList<Cancion> getCanciones(){
		return canciones;
	}
	
	
	public void generarPrueba(){
		nombre = "Pepe";
		nacionalidad = "Colombiano";
		paginaWeb = "www.pepeweb.com.co";
		Cancion c1 = new Cancion("Prismo","https://www.youtube.com/watch?v=A7E-jPMolJ0");
		Cancion c2 = new Cancion("T-mass","https://www.youtube.com/watch?v=xzX4PWZT3A0");
		agregarCanción(c1);
		agregarCanción(c2);
	}
	
	
}
